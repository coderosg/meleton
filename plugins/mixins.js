/* ============
 * Global mixins
 * ============
 *
 */

import Vue from 'vue'

const globalMixins = {
  filters: {
    slice(text, length) {
      if (typeof text !== 'string') return
      if (text.length >= length) {
        return text.slice(0, length) + '...'
      }
      return text
    },
    money(price) {
      if (price) {
        return price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ' ')
      }
      return '0'
    },
  },
}

Vue.mixin(globalMixins)
