const data = require('@/static/data.json')
export const state = () => ({
  allList: data.courses,
  list: null,
  detail: null,
})

export const mutations = {
  SET_COURSE_DETAIL(state, payload) {
    state.detail = payload
  },
  SET_COURSE_LIST(state, payload) {
    state.list = payload
  },
  ADD_COURSE(state, payload) {
    state.allList.push(payload)
  },
  DELETE_COURSE(state, index) {
    state.allList.splice(index, 1)
  },
  UPDATE_COURSE(state, { index, payload }) {
    state.allList[index] = payload
  },
}

export const actions = {
  getCourseDetailAction({ commit, state }, id) {
    const detail = state.allList.find((item) => item.id === id)
    const startDate = new Intl.DateTimeFormat('fr-ca').format(
      new Date(detail.startDate)
    )
    commit('SET_COURSE_DETAIL', {
      ...detail,
      startDate,
    })
  },
  addCourseAction({ commit, state }, { startDate, ...detail }) {
    let id = 1
    const allList = state.allList
    if (allList.length) {
      id = allList[allList.length - 1].id + 1
    }
    commit('ADD_COURSE', {
      id,
      ...detail,
      startDate: new Date(startDate),
    })
  },
  updateCourseAction({ commit, state }, { id, startDate, ...payload }) {
    const index = state.allList.findIndex((item) => item.id === id)
    commit('UPDATE_COURSE', {
      index,
      payload: {
        id,
        ...payload,
        startDate: new Date(startDate),
      },
    })
  },
  deleteCourseAction({ commit, state }, id) {
    const index = state.allList.findIndex((item) => item.id === id)
    commit('DELETE_COURSE', index)
  },
  getCourseListActions({ commit, state }, params = {}) {
    /* eslint-disable */
    const { price, startDate, page = 1, pageSize = 5 } = params
    let allList = [...state.allList]
    if (startDate === '-startDate') {
      allList = allList.sort((a, b) => {
        return new Date(b.startDate) - new Date(a.startDate)
      })
    } else if(startDate === 'startDate') {
      allList = allList.sort((a, b) => {
        return new Date(a.startDate) - new Date(b.startDate)
      })
    }
    if (price === '-price') {
      allList = allList.sort((a, b) => {
        return b.price - a.price
      })
    } else if(price === 'price') {
      allList = allList.sort((a, b) => {
        return a.price - b.price
      })
    }
    const count = allList.length
    const from = (page - 1) * pageSize
    const to = page * pageSize > count ? count : page * pageSize
    let data = allList.slice(from, to)

    commit('SET_COURSE_LIST', {
      page,
      pageSize,
      data,
      count,
    })
  },
}

export const getters = {}
