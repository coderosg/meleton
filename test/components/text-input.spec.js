import { mount } from '@vue/test-utils'
import TextInput from '@/components/core/TextInput.vue'

describe('TextInput.vue', () => {
  test('text input props value', () => {
    const value = 'Text input'
    const wrapper = mount(TextInput, {
      propsData: { valid: true, value },
    })
    const elValue = wrapper.find('input').element.value
    expect(wrapper.find('.input').classes()).toContain('valid')
    expect(elValue).toBe(value)
  })

  test('text input set value', () => {
    const value = 'Text input'
    const wrapper = mount({
      components: {
        TextInput,
      },
      data() {
        return {
          input: '',
        }
      },
      template: `
        <div>
          <text-input v-model="input" />
        </div>
      `,
    })
    wrapper.find('input').setValue(value)
    expect(wrapper.vm.input).toBe(value)
  })
})
